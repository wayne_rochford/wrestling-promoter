require 'test_helper'

class WrestlersControllerTest < ActionController::TestCase
  setup do
    @wrestler = wrestlers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:wrestlers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create wrestler" do
    assert_difference('Wrestler.count') do
      post :create, wrestler: { age: @wrestler.age, division: @wrestler.division, image_url: @wrestler.image_url, real_name: @wrestler.real_name, turn: @wrestler.turn, wage: @wrestler.wage, wrestling_name: @wrestler.wrestling_name }
    end

    assert_redirected_to wrestler_path(assigns(:wrestler))
  end

  test "should show wrestler" do
    get :show, id: @wrestler
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @wrestler
    assert_response :success
  end

  test "should update wrestler" do
    patch :update, id: @wrestler, wrestler: { age: @wrestler.age, division: @wrestler.division, image_url: @wrestler.image_url, real_name: @wrestler.real_name, turn: @wrestler.turn, wage: @wrestler.wage, wrestling_name: @wrestler.wrestling_name }
    assert_redirected_to wrestler_path(assigns(:wrestler))
  end

  test "should destroy wrestler" do
    assert_difference('Wrestler.count', -1) do
      delete :destroy, id: @wrestler
    end

    assert_redirected_to wrestlers_path
  end
end

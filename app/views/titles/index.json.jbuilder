json.array!(@titles) do |title|
  json.extract! title, :id, :name, :division, :prestige, :image_url, :wrestler_id
  json.url title_url(title, format: :json)
end

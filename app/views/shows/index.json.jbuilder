json.array!(@shows) do |show|
  json.extract! show, :id, :name, :running_time, :grade, :promotion_id
  json.url show_url(show, format: :json)
end

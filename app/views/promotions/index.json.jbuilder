json.array!(@promotions) do |promotion|
  json.extract! promotion, :id, :name, :size, :established, :cash
  json.url promotion_url(promotion, format: :json)
end

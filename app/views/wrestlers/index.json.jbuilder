json.array!(@wrestlers) do |wrestler|
  json.extract! wrestler, :id, :real_name, :wrestling_name, :age, :division, :turn, :wage, :image_url
  json.url wrestler_url(wrestler, format: :json)
end

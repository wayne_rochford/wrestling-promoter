class CreateShows < ActiveRecord::Migration
  def change
    create_table :shows do |t|
      t.string :name
      t.string :running_time
      t.string :grade
      t.references :promotion, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

class AddAttachmentImageToWrestlers < ActiveRecord::Migration
  def self.up
    change_table :wrestlers do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :wrestlers, :image
  end
end

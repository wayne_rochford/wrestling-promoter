class CreateWrestlers < ActiveRecord::Migration
  def change
    create_table :wrestlers do |t|
      t.string :real_name
      t.string :wrestling_name
      t.integer :age
      t.string :division
      t.string :turn
      t.decimal :wage
      t.string :image_url

      t.timestamps null: false
    end
  end
end

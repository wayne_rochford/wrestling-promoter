class RemoveImageUrlFromTitles < ActiveRecord::Migration
  def change
    remove_column :titles, :image_url, :string
  end
end

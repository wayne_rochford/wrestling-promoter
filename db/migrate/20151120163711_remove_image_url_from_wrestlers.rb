class RemoveImageUrlFromWrestlers < ActiveRecord::Migration
  def change
    remove_column :wrestlers, :image_url, :string
  end
end

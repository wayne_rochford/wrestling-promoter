class CreateTitles < ActiveRecord::Migration
  def change
    create_table :titles do |t|
      t.string :name
      t.string :division
      t.string :prestige
      t.string :image_url
      t.references :wrestler, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

class AddAttachmentImageToTitles < ActiveRecord::Migration
  def self.up
    change_table :titles do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :titles, :image
  end
end

class CreatePromotions < ActiveRecord::Migration
  def change
    create_table :promotions do |t|
      t.string :name
      t.string :size
      t.string :established
      t.decimal :cash

      t.timestamps null: false
    end
  end
end

# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151120172747) do

  create_table "matches", force: :cascade do |t|
    t.integer  "show_id"
    t.string   "match_category"
    t.integer  "wrestler_id"
    t.integer  "title_id"
    t.string   "notes"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "matches", ["show_id"], name: "index_matches_on_show_id"
  add_index "matches", ["title_id"], name: "index_matches_on_title_id"
  add_index "matches", ["wrestler_id"], name: "index_matches_on_wrestler_id"

  create_table "promotions", force: :cascade do |t|
    t.string   "name"
    t.string   "size"
    t.string   "established"
    t.decimal  "cash"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "shows", force: :cascade do |t|
    t.string   "name"
    t.string   "running_time"
    t.string   "grade"
    t.integer  "promotion_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "shows", ["promotion_id"], name: "index_shows_on_promotion_id"

  create_table "titles", force: :cascade do |t|
    t.string   "name"
    t.string   "division"
    t.string   "prestige"
    t.integer  "wrestler_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  add_index "titles", ["wrestler_id"], name: "index_titles_on_wrestler_id"

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

  create_table "wrestlers", force: :cascade do |t|
    t.string   "real_name"
    t.string   "wrestling_name"
    t.integer  "age"
    t.string   "division"
    t.string   "turn"
    t.decimal  "wage"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

end
